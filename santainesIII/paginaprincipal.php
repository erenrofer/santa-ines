
<?php
include 'php/establecer_conexion.php';
session_start();
if (!isset ($_SESSION['cuenta'])){

    echo ' <script>
    ;
    window.location = "index.php";
    </script>
    ';
    session_destroy();
    die;
}

 
$cuenta = $_SESSION['cuenta'] ;

$datos = mysqli_query ($conexion, "SELECT usuarios.nombrec, usuarios.mail, usuarios.CI, roles.Nombre as rol FROM usuarios
left join roles ON usuarios.rol_id = roles.id   
WHERE CI='$cuenta'");


$resultado = $datos->fetch_assoc();
if(mysqli_num_rows ($datos) >0){

    $ROL = $resultado['rol'];
}
$rolesadmitidos = ['administrador'];
if((in_array($ROL, $rolesadmitidos))){
    echo ' <script> alert("bienvenido admin")
    ;
    </script>;  ';
}
else

{
    echo
        ' <script>
        alert ("Bienvenido usuario");
        </script>
        ';
}   
   

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Santa Ines</title>
    <link rel="stylesheet" href="cssprincipal/normalize.css">
    <link rel="stylesheet" href="cssprincipal/estilos.css">
</head>

<body>

    <header class="hero">
        <nav class="nav container">
            <div class="nav__logo">
                <h2 class="nav__title">Santa Ines.</h2>
            </div>

            <ul class="nav__link nav__link--menu">
                <li class="nav__items">
                    <a href="paginaprincipal.php" class="nav__links">Inicio</a>
                </li>
                <li class="nav__items">
                    <a href="historia/comunidad.html" class="nav__links">Acerca de</a>
                </li>
                <li class="nav__items">
                    <a href="historia/contactos.html" class="nav__links">Contactos</a>
                </li>
                <li class="nav__items">
                <a href= "php/cerrarsesion.php" class="closet"> Cerrar sesión </a>
                </li>
            </ul>
        </nav>

        <section class="hero__container container">
            <h1 class="hero__title">Bienvenido a la Web del Consejo Comunal Santa Ines.</h1>
            <p class="hero__paragraph">Siempre construyendo un futuro mejor.</p>
        </section>
    </header>

    <main>
        <section class="container about">
            <h2 class="subtitle">Aquí podrás solicitar documentos necesarios</h2>

            <div class="about__main">
                <article class="about__icons">
                    <img src="./imagenesprincipal/pdf.png" class="about__icon">
                    <h3 class="about__title">Carta de Residencia</h3>
                    <a href="formulario/formulario.php" target="_blank" class="cta">Solicitar</a>
                </article>

            </div>
        </section>

        <section class="knowledge">
            <div class="knowledge__container container">
                <div class="knowledege__texts">
                    <h2 class="subtitle">Conoce sobre las Funciones de un consejo Comunal</h2>
                </div>

                <figure class="knowledge__picture">
                    <img src="./imagenesprincipal/consejocomunal.png">
                </figure>
            </div>
        </section>


        <section class="questions container">
            <h2 class="subtitle">Preguntas frecuentes</h2>
            <section class="questions__container">
                <article class="questions__padding">
                    <div class="questions__answer">
                        <h3 class="questions__title">¿Qué es el Consejo Comunal?
                            <span class="questions__arrow">
                                <img src="./imagenesprincipal/arrow.svg" class="questions__img">
                            </span>
                        </h3>

                        <p class="questions__show">Los Consejos Comunales (CC) son entes representativos y deliberativos
                             de participación ciudadana a través de los cuales las comunidades proponen, debaten, formulan, 
                             deciden, gestionan y evalúan proyectos de política pública. Cuentan con unidades de administración 
                             y financiación, controlaría social y ejecución. Pueden estar formados por entre 150 y 400 familias
                              en áreas urbanas, por un mínimo de 20 familias en áreas rurales, y de 10 familias en comunidades 
                              indígenas.</p>
                    </div>
                </article>

                <article class="questions__padding">
                    <div class="questions__answer">
                        <h3 class="questions__title">¿Cual es la Funcion de un Consejo Comunal?
                            <span class="questions__arrow">
                                <img src="./imagenesprincipal/arrow.svg" class="questions__img">
                            </span>
                        </h3>

                        <p class="questions__show">Para su funcionamiento, una asamblea de la comunidad elige a unos voceros o representantes 
                              que deben recibir el 30 % de los votos de los adultos mayores de quince años o del 20 % en 
                              una segunda vuelta. Estos representantes se encargan de conformar comités de trabajo y pueden 
                              servir por dos años con la posibilidad de ser re-electos y revocados por la asamblea. Los comités de trabajo 
                              asumen funciones de salud, familia y género, economía, seguridad, vivienda, medios alternativos, y educación, 
                              entre otros asuntos. Las decisiones finales se toman en la asamblea de ciudadanos y deben ser refrendadas con 
                              la presencia de al menos un 30 % de los residentes o del 20 % en una segunda vuelta. Su financiación depende 
                              directamente del Gobierno Central, que ha transferido billones de bolívares fuertes a estas instancias. 
                              En 2009 había cerca de 30.000 consejos comunales y se estima que más de ocho millones de personas 
                              han participado en ellos.</p>
                    </div>
                </article>

                <article class="questions__padding">
                    <div class="questions__answer">
                        <h3 class="questions__title">¿Cúantos Comites Conforman a un Consejo Comunal?
                            <span class="questions__arrow">
                                <img src="./imagenesprincipal/arrow.svg" class="questions__img">
                            </span>
                        </h3>

                        <p class="questions__show">En su estructura posee comités: 
                            agrario, de Tierras, vivienda y hábitat, Salud, Seguridad Integral; 
                            Educación, Alimentación; Protección e igualdad social, Comité de agua, 
                            energía, deporte y recreación, economía popular.</p>
                    </div>
                </article>
            </section>
        </section>
    </main>

    <footer class="footer">
        <section class="footer__copy container">
            <h3 class="footer__copyright">Derechos reservados &copy; Consejo comunal Santa Ines</h3>
        </section>
    </footer>
    
    
                </hr>
    <script src="./jsprincipal/slider.js"></script>
    <script src="./jsprincipal/questions.js"></script>
    <script src="./jsprincipal/menu.js"></script>
</body>

</html>